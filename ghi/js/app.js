function createCard(title, description, picture_url, starts, ends, place) {
    return `
    <div class="col-4">
    <div class="card shadow-sm p-3 mb-5 bg-body-tertiary rounded grid gap-3">
    <img src="${picture_url}" class="card-img-top">
    <div class="card-body">
      <h5 class="card-title">${title}</h5>
      <h6 class="card-subtitle mb-2 text-muted">${place}</h6>
      <p class="card-text">${description}</p>
    </div>
    <div class="card-footer text-body-secondary">${starts}-${ends}</div>
  </div>
  </div>
`;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok');
        } else {
        const data = await response.json();

        for (let conference of data.conferences){

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const picture_url = details.conference.location.picture_url
          const startschange = new Date(details.conference.starts)
          const starts = new Intl.DateTimeFormat('en-US').format(startschange)
          const endschange = new Date(details.conference.ends)
          const ends = new Intl.DateTimeFormat('en-us').format(endschange)
          const place = details.conference.location.name
          const html = createCard(title,description,picture_url,starts,ends, place)
          const column = document.querySelector('.row')
          column.innerHTML += html
        }
        }
    }
}catch (e) {
        console.error(e);
        console.log('there was a problem with your request');
      }
  })
